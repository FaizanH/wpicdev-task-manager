$(document).ready(function() {
  document.getElementById('file-input')
  .addEventListener('change', readSingleFile, false);
});

function readSingleFile(e) {
  var file = e.target.files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    /* convert data to binary string */
    var data = new Uint8Array(contents);;

    var arr = new Array();
    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    var bstr = arr.join("");

    /* Call XLSX */
    var workbook = XLSX.read(bstr, {
        type: "binary"
    });

    /* DO SOMETHING WITH workbook HERE */
    var first_sheet_name = workbook.SheetNames[0];
    /* Get worksheet */
    var worksheet = workbook.Sheets[first_sheet_name];
    console.log(
      XLSX.utils.sheet_to_json(worksheet, {raw: true})
    );

    var table = new Tabulator("#grid", {
      layout:"fitColumns",
      autoColumns:true,
      resizableColumns:false,
      data:XLSX.utils.sheet_to_json(worksheet, {raw: true}),
    })
  };

  reader.readAsArrayBuffer(file);
}