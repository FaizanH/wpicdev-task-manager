
  jQuery(".chosen-select").chosen(); // Apply jQuery autocomplete plugin

  jQuery('select').on("change", function() { // Handle Selection box click
    var content_id = jQuery(this).val()
    console.log(content_id);
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://cors-anywhere.herokuapp.com/https://www.bosscoauto.com.au/do/WS/NetoAPI", // Set up Proxy for CORS-enabled request
      "method": "POST",
      "headers": {
        "netoapi_action": "GetContent",
        "netoapi_username": "",
        "netoapi_key": "",
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      data: "{\r\n   \"Filter\": {\r\n    \"ContentID\": [" + content_id + "],\r\n    \"OutputSelector\":[\"Description2\"]\r\n   }\r\n}" // Add ContentID variable to data object
    }
    jQuery.ajax(settings).done(function (response) {
      jQuery(response).find('GetContentResponse').each(function() {  // Filter GetContentResponse node
        jQuery(this).find('Content').each(function() {  // Filter Content node
          jQuery(this).find('Description2').each(function() { // Filter Description2 node
            var desc = jQuery("option:selected", jQuery("#bookly-vehicle")).text() + ' ---- ';
            var html_desc = jQuery(this).text();
            desc += jQuery(html_desc).text(); // Get HTML Text from object
            console.log(desc);
            jQuery('#neto-note').val(desc);
          });
        });
      });
    });
  });
  