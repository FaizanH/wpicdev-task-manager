  // while looping endlessly because it cant detect change in start/end values in time
  // while (start <= end && !found) { // Search through pages first
  //   midPage = Math.floor(parseInt(start + end) / 2);
  //   //var done = await getData(midPage);
  //   getData(midPage)
  //   .then((response) => {
  //     var midPageData = response.data; // Search Mid Data. NOTE - Async call cannot set global variable
  //     console.log(response);

  //     // Search through data on page | The one binary search retrieved
  //     var obj = {"id" : {},"added" : {},"type" : {},"value" : {},"tag" : {},"name" : {}};

  //     /* Binary search on page data */
  //     var startData = Object.keys(midPageData)[0];
  //     var endData = Object.keys(midPageData).length-1;
  //     var mid;

  //     while (startData <= endData) {
  //       mid = Math.floor(parseInt(startData + endData) / 2); //(startData + (endData - startData) >> 1);
  //       console.log("Start data: "+startData+" ,End Data: "+endData+" ,Mid Data: "+mid);

  //       // Search through mid page for data match
  //       if (filterKey === midPageData[mid].id.toUpperCase()) { // response.data[midIndex].id.toUpperCase()
  //         found = true; // ASYNC CALL
  //         /* Add to found array */
  //         var foundVal = midPageData[mid].id;
  //         //console.log(filterKey+" == "+response.data[midData].id.toUpperCase());
  //         //console.log("Response: "+response.data[midData].id.toUpperCase()+", Found (==): "+found+", Start: "+startData+", End: "+endData+" ,Mid: "+midData);
  //         break;
  //       }
  //       else if (filterKey > midPageData[mid].id.toUpperCase()) {
  //         endData = mid - 1; // set new data position index
  //         found = false; // ASYNC CALL
  //         //console.log(filterKey+" > "+response.data[midData].id.toUpperCase());
  //         //console.log("Response: "+response.data[midData].id.toUpperCase()+", Found (>): "+found+", Start: "+startData+", End: "+endData+" ,Mid: "+midData);
  //       } else {
  //         startData = mid + 1;
  //         found = false; // ASYNC CALL
  //         //console.log(filterKey+" < "+response.data[midData].id.toUpperCase());
  //         //console.log("Response: "+response.data[midData].id.toUpperCase()+", Found (<): "+found+", Start: "+startData+", End: "+endData+" ,Mid: "+midData);
  //       }
  //     }

  //     if (found) {
  //       console.log("Found value");
  //       midPageData.filter(function(dat, index){
  //         // if match add to other response fields to results array
  //         if (dat.id == foundVal) {
  //           obj.id = dat.id;
  //           obj.added = dat.added;
  //           obj.type = dat.type;
  //           obj.value = dat.value;
  //           obj.tag = dat.tag;
  //           obj.name = dat.name;
  //           searchResultsDataJSON.data.splice(index, 0, obj); // Insert new object - ASYNC CALL
  //         }
  //       });
  //     }
  //     else { // Halt all processes until this is complete - semaphore/mutex
  //       console.log("Value NOT found on page: "+response.page);
  //       // Set start/end page indexes
  //       if(filterKey > midPageData[mid].id.toUpperCase()) {
  //         end = response.page - 1; // ASYNC CALL
  //       } else {
  //         start = response.page + 1; // ASYNC CALL
  //       }
  //       console.log("Start: "+start+", End: "+end);
  //     }
  //     console.log(searchResultsDataJSON);
  //     // return found, start, end
  //     console.log("Start: "+start+", End: "+end+" ,Found: "+found);
  //   })
  //   .catch((e) => {
  //     //some error
  //   })

    // // Linear Search - Works but inefficient
  // // for(var i=1;i<=2;i++) {
  // //   if(filter != '')
  // //     searchLoad(i, filter);
  // //   else if(filter == '')
  // //     fetchData(PAGE_NUM_GLOBAL); // Normal results (search disabled)
  // // }
  // }

  function getMidPageData(AJAXresponse) {
  var midPageData = AJAXresponse.data; // Search Mid Data. NOTE - Async call cannot set global variable
  console.log(AJAXresponse);

  // Search through data on page | The one binary search retrieved
  var obj = {"id" : {},"added" : {},"type" : {},"value" : {},"tag" : {},"name" : {}};

  /* Binary search on page data */
  var startData = Object.keys(midPageData)[0];
  var endData = Object.keys(midPageData).length-1;
  var mid;

  while (startData <= endData) {
    mid = Math.floor(parseInt(startData + endData) / 2); //(startData + (endData - startData) >> 1);
    console.log("Start data: "+startData+" ,End Data: "+endData+" ,Mid Data: "+mid);

    // Search through mid page for data match
    if (filterKey === midPageData[mid].id.toUpperCase()) { // response.data[midIndex].id.toUpperCase()
      found = true; // ASYNC CALL
      /* Add to found array */
      var foundVal = midPageData[mid].id;
      //console.log(filterKey+" == "+response.data[midData].id.toUpperCase());
      //console.log("Response: "+response.data[midData].id.toUpperCase()+", Found (==): "+found+", Start: "+startData+", End: "+endData+" ,Mid: "+midData);
      break;
    }
    else if (filterKey > midPageData[mid].id.toUpperCase()) {
      endData = mid - 1; // set new data position index
      found = false; // ASYNC CALL
      //console.log(filterKey+" > "+response.data[midData].id.toUpperCase());
      //console.log("Response: "+response.data[midData].id.toUpperCase()+", Found (>): "+found+", Start: "+startData+", End: "+endData+" ,Mid: "+midData);
    } else {
      startData = mid + 1;
      found = false; // ASYNC CALL
      //console.log(filterKey+" < "+response.data[midData].id.toUpperCase());
      //console.log("Response: "+response.data[midData].id.toUpperCase()+", Found (<): "+found+", Start: "+startData+", End: "+endData+" ,Mid: "+midData);
    }
  }

  if (found) {
    console.log("Found value");
    midPageData.filter(function(dat, index){
      // if match add to other response fields to results array
      if (dat.id == foundVal) {
        obj.id = dat.id;
        obj.added = dat.added;
        obj.type = dat.type;
        obj.value = dat.value;
        obj.tag = dat.tag;
        obj.name = dat.name;
        searchResultsDataJSON.data.splice(index, 0, obj); // Insert new object - ASYNC CALL
      }
    });
  }
  else { // Halt all processes until this is complete - semaphore/mutex
    console.log("Value NOT found on page: "+AJAXresponse.page);
    // Set start/end page indexes
    if(filterKey > midPageData[mid].id.toUpperCase()) {
      end = AJAXresponse.page - 1; // ASYNC CALL
    } else {
      start = AJAXresponse.page + 1; // ASYNC CALL
    }
    console.log("Start: "+start+", End: "+end);
  }
  console.log(searchResultsDataJSON);
  // return found, start, end
  // return new Promise(function(resolve, reject) {
  //   resolve([found, start, end]);
  // });
}

  //   // do a promise.then here | DONT MIX PROMISE AND CALLBACKS
  //   /* AJAX CALLBACK METHOD */
  //   // midPage = Math.floor(parseInt(start + end) / 2);
  //   // AJAXCall(midPage, getMidPageData);
  //   // console.log("MIDDLE PAGE: "+ midPage);
  //   // console.log("Start: "+start+", End: "+end+" ,Found: "+found);