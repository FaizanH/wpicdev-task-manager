# WPIC Tasks List #

## Task List API Tool
### How to run
```
Run website on server.
```

```
Features:
- Responsive design
- Pagination
- Binary Search
- Uses Bootstrap framework
- Loading screen based off Ajax request
```
## Rev1.1 20/11/19
![Top Screen](https://bitbucket.org/FaizanH/wpicdev-task-manager/raw/b9992acf257df8c08d163ea736be4d0a8db8b820/assets/s1.png)
![Bottom Screen](https://bitbucket.org/FaizanH/wpicdev-task-manager/raw/b9992acf257df8c08d163ea736be4d0a8db8b820/assets/s2.png)
## Rev1.2 3/12/19
![Load Screen](https://bitbucket.org/FaizanH/wpicdev-task-manager/raw/c1b53aac590083b717b63317ba0f22cad75fe185/assets/load.png)
![Main Screen](https://bitbucket.org/FaizanH/wpicdev-task-manager/raw/c1b53aac590083b717b63317ba0f22cad75fe185/assets/menu.png)

---
## CSV Extractor Tool
### How to run
```
Run website on server.
```

```
Features:
- Fully-responsive
- XLSX, CSV Support
- Cross-browser support
- Uses Tabulator & SheetJS fork
- Upload local files
```
![Main Screen](https://bitbucket.org/FaizanH/wpicdev-task-manager/raw/ff0fb94be0a343e5ca8d62723121f2fec289440c/csv-extractor/assets/screens/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20191219164321.png)

---
## Store Generator Tool
### How to run
```
Ensure you have the latest version of node installed (Run npm update)

Edit #TPL_username_1 & #TPL_password_1 with your personal login details.

Run program by navigating to directory:
(Windows)
> node .\taobao.js
```

```
Features:
- NightmareJs automation script
- Dependency injection support
- Uses Electron browser
- Local file storage support
```
![Main Screen](https://bitbucket.org/FaizanH/wpicdev-task-manager/raw/94af812e66716212050bd957567991a92aae741e/taobao-tools/assets/main-data.png)