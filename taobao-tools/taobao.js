const Nightmare = require('nightmare')
const path = require('path');
const fs = require('fs');

const nightmare = Nightmare({
  // webPreferences: {
  //   preload: path.resolve('get_tmall.js')
  //   // preload: "absolute path to/get_tmall.js"
  // },
  show: true
})

/* Dependencies */
var fileData = [ [], [], [] ];

fileData[0].push(fs.readFileSync(path.resolve('get_tmall.js'), 'utf-8'));
fileData[1].push(fs.readFileSync(path.resolve('jquery.min.js'), 'utf-8'));
fileData[2].push(fs.readFileSync(path.resolve('moment.min.js'), 'utf-8'));

nightmare
  .goto('https://login.taobao.com/member/login.jhtml?from=sycm&full_redirect=true&style=minisimple&minititle=&minipara=0,0,0&sub=true&redirect_url=http://sycm.taobao.com/')
   .wait('#TPL_username_1')
   .wait('#TPL_password_1')
   .insert('#TPL_username_1', 'bluenile旗舰店:wpic')
   .insert('#TPL_password_1', 'wpic8888')
   .click('#J_SubmitStatic')
   .wait(5000)
    /* Get date information - Look at get_tmall.js */
    //.goto('https://sycm.taobao.com/qos/review/overview/guide.json?dateRange='+ '2020-01-01' +'|' + '2020-01-01' + '&dateType=day')
   .evaluate((fileData) => {
      var elem1, elem2, elem3 = null;

      for(var ii=0;ii<fileData[0].length; ii++ ) {
        elem1 = document.createElement('script');
        if (elem1) {
          elem1.setAttribute('type', 'text/javascript');
          elem1.innerHTML = fileData[0][ii];
          document.head.appendChild(elem1);
        }
      }
      for(var iii=0;iii<fileData[1].length; iii++ ) {
        elem2 = document.createElement('script');
        if (elem2) {
          elem2.setAttribute('type', 'text/javascript');
          elem2.innerHTML = fileData[1][iii];
          document.head.appendChild(elem2);
        }
      }
      for(var iv=0;iv<fileData[2].length; iv++ ) {
        elem3 = document.createElement('script');
        if (elem3) {
          elem3.setAttribute('type', 'text/javascript');
          elem3.innerHTML = fileData[2][iv];
          document.head.appendChild(elem3);
        }
      }
      /* Dependencies injected
      * Run code here */
    //  if getTmallAll() 
      return getTmallAll();

      /* End of nightmare script */
      // return document.body.innerText;

    }, fileData)
    .then((response) => {
      console.log(response);
    })
  // .end() // Add to production script
  .catch(error => {
    console.error('Search failed:', error)
  })