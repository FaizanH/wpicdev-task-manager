function getTmallAll() {

    function shopInfo() {
        var apiPath = 'https://sycm.taobao.com/custom/menu/getPersonalView.json';
        $.ajax({
            url: apiPath,
            method: "GET",
            contentType: "application/json",
            success: function(data) {
                var shopDesc = data;
                console.log("店铺信息加载成功:");
                console.log(data);
                storeObj.storeName = shopDesc.data.mainUserName;
                storeObj.merchantId = shopDesc.data.mainUserId;
                storeObj.storeId = shopDesc.data.runAsShopId;
                var storeId = storeObj.storeId;
                if(localStoreData.tmall === null || localStoreData["tmall"][storeId] !== chooseDate){
                    dsr();
                    csInquire();
                    traffics();
                    getTmallSales();
                }else{
                    needUpdate = false;
                    console.log("%c Data has been submitted!","background:lightgreen;color:#fff");
                }
            }
        }).fail(function(jqXHR, textStatus) {
            console.log("Request failed: " + textStatus + "," + JSON.stringify(jqXHR));
            if (jqXHR.status == 404) {
                console.log("请求失败")
            } else if (jqXHR.status == 0 && jqXHR.statusText == "error") {
                console.log("请求失败，有错误！")
            }
        });
    }

    function dsr() {
        // 描述评论
        var apiPath = 'https://sycm.taobao.com/qos/review/overview/guide.json?dateRange=' + chooseDate + '|' + chooseDate + '&dateType=day';
        // https://sycm.taobao.com/qos/review/overview/trend.json?dateType=day&dateRange=2019-12-12%7C2019-12-12,30天数据

        $.ajax({
            url: apiPath,
            method: "GET",
            contentType: "application/json",
            success: function(data) {
                var rateDesc = data;
                console.log("描述评论获取成功:");
                console.log(data);
                storeObj.rateDesCompare = rateDesc["data"]["itemDsr"]["value"]; //描述相符评分
                storeObj.rateCSService = rateDesc["data"]["serviceDsr"]["value"]; //卖家服务评分
                storeObj.rateLogistics = rateDesc["data"]["logisticsDsr"]["value"]; //物流服务评分
            }
        }).fail(function(jqXHR, textStatus) {
            console.log("Request failed: " + textStatus + "," + JSON.stringify(jqXHR));
            if (jqXHR.status == 404) {
                console.log("DSR数据请求失败")
            } else if (jqXHR.status == 0 && jqXHR.statusText == "error") {
                console.log("DSR数据请求失败，有错误！")
            }
        });
    }

    function csInquire() {
        // 咨询人数及访客量
        var apiPath = 'https://sycm.taobao.com/qos/portal/service/consulting/overview.json?dateType=day&dateRange=' + chooseDate + '%7C' + chooseDate + '&indexCode=uv%2CconsultingUv%2ConSubAccountCnt%2CconsultRate';
        $.ajax({
            url: apiPath,
            method: "GET",
            contentType: "application/json",
            success: function(data) {
                var inquireDesc = data;
                console.log("店铺信息加载成功:");
                console.log(data);
                storeObj.csInquiries = inquireDesc["data"]["consultingUv"]["value"]; //咨询人数
                storeObj.trafficUV = inquireDesc["data"]["uv"]["value"]; //昨日访客
            }
        }).fail(function(jqXHR, textStatus) {
            console.log("Request failed: " + textStatus + "," + JSON.stringify(jqXHR));
            if (jqXHR.status == 404) {
                console.log("访客数据请求失败")
            } else if (jqXHR.status == 0 && jqXHR.statusText == "error") {
                console.log("访客数据请求失败，有错误！")
            }
        });
    }

    function traffics() {
        console.log("开始获取流量");

        function findKey(keyName, obj) {
            var objChild = obj["children"];
            for (var i = 0; i < objChild.length; i++) {
                var deepChild = objChild[i];
                for (var a in deepChild) {
                    if (a == "pageName") {
                        var get = deepChild[a].value;
                        if (get == keyName) {
                            var resultObj = deepChild.uv.value;
                            return resultObj;
                        }
                    }
                }
            }
        }

        function findGroup(keyName, obj) {
            for (var i = 0; i < obj.length; i++) {
                var deepChild = obj[i];
                for (var a in deepChild) {
                    if (a == "pageName") {
                        var get = deepChild[a].value;
                        if (get == keyName) {
                            console.log("findGroup:" + get)
                            return deepChild;
                        }
                    }
                }
            }
        }

        var trafficObj = {
            wuxian: -1,
            pc: -1
        }

        function mobileTraffics() {
            // 无线端访问数据
            var apiPath = 'https://sycm.taobao.com/flow/v3/shop/source/tree.json?belong=all&dateRange=' + chooseDate + '%7C' + chooseDate + '&dateType=recent1&device=2&indexCode=uv,crtByrCnt,crtRate&order=desc&orderBy=uv&page=1&pageSize=10';
            $.ajax({
                url: apiPath,
                method: "GET",
                contentType: "application/json",
                success: function(data) {
                    var trafficDesc = data;
                    console.log("无线端访问数据:");
                    console.log(data);
                    var obj_data = findGroup("淘内免费", trafficDesc.data);
                    // var maoke = findKey("猫客搜索", obj_data); //猫客搜索
                    var maoke = findKey("手猫搜索", obj_data); //猫客搜索, 2019.12.13， 更新数据关键词，手机天猫客户端搜索量
                    var shoutao = findKey("手淘搜索", obj_data); //手淘搜索
                    trafficObj.wuxian = parseInt(maoke) + parseInt(shoutao);
                }
            }).fail(function(jqXHR, textStatus) {
                console.log("Request failed: " + textStatus + "," + JSON.stringify(jqXHR));
                if (jqXHR.status == 404) {
                    console.log("请求失败")
                } else if (jqXHR.status == 0 && jqXHR.statusText == "error") {
                    console.log("请求失败，有错误！")
                }
            });
        }

        function pcTraffics() {
            // pc端访问数据
            var apiPath = 'https://sycm.taobao.com/flow/v3/shop/source/tree.json?belong=all&dateRange=' + chooseDate + '%7C' + chooseDate + '&dateType=recent1&device=1&indexCode=uv,crtByrCnt,crtRate&order=desc&orderBy=uv&page=1&pageSize=10';
            $.ajax({
                url: apiPath,
                method: "GET",
                contentType: "application/json",
                success: function(data) {
                    var pc_obj = data;
                    console.log("pc端访问数据:");
                    console.log(data);
                    var pc_data = findGroup("淘内免费", pc_obj.data);
                    var taobao = findKey("淘宝搜索", pc_data); //淘宝搜索
                    var tmall = findKey("天猫搜索", pc_data); //天猫搜索
                    trafficObj.pc = parseInt(taobao) + parseInt(tmall);
                }
            }).fail(function(jqXHR, textStatus) {
                console.log("Request failed: " + textStatus + "," + JSON.stringify(jqXHR));
                if (jqXHR.status == 404) {
                    console.log("请求失败")
                } else if (jqXHR.status == 0 && jqXHR.statusText == "error") {
                    console.log("请求失败，有错误！")
                }
            });
        }

        mobileTraffics();
        pcTraffics();

        waitFor(function() {
            return trafficObj.wuxian !== -1 && trafficObj.pc !== -1;
        }, function() {
            storeObj.trafficOSV = trafficObj.wuxian + trafficObj.pc;
            console.log("流量数据获取成功！");
        }, function() {
            console.log("流量数据获取失败");
            console.log(trafficObj);
        }, 20000);
    }

    function getTmallSales(){
        console.log("开始获取销售数量");
        var apiPath = 'https://sycm.taobao.com/portal/coreIndex/getShopMainIndexes.json?dateType=day&dateRange=' + chooseDate + '%7C' + chooseDate + '&device=0';
        $.ajax({
            url: apiPath,
            method: "GET",
            contentType:"application/json",
            success:function(res){
                console.log("销售数据获取成功");
                console.log(res);
                var sales = res.content.data;
                storeObj.orderCount = sales.payOrdCnt.value;
                storeObj.orderPayment = sales.payAmt.value;
            }
        }).fail(function(jqXHR, textStatus) {
            console.log("Request failed: " + textStatus + "," + JSON.stringify(jqXHR));
            if (jqXHR.status == 404) {
                console.log("销售数据请求失败")
            } else if (jqXHR.status == 0 && jqXHR.statusText == "error") {
                console.log("销售数据请求失败，有错误！")
            }
        });
    }

    shopInfo();

    waitFor(function() {
        return storeObj.storeName !== null && storeObj.merchantId !== null && storeObj.storeId !== null && storeObj.rateDesCompare !== null && storeObj.rateCSService !== null && storeObj.rateLogistics !== null && storeObj.csInquiries !== null && storeObj.trafficUV !== null && storeObj.trafficOSV !== null && storeObj.recordDate !== null && storeObj.orderCount !== null && storeObj.orderPayment !== null;
    }, function() {
        console.log("Posting Tmall Data:");
        storeObj.createTime = moment.utc().add(8, 'hours').format('YYYY-MM-DD HH:mm:ss');
        storeObj.storeType = "tmall";
        console.log(storeObj);
        var filename = chooseDate + "_storeKPI";
        postData2File(storeObj, filename);

        // 写入本地存储
        var thisObj = {};
        thisObj[storeObj.storeId] = chooseDate;
        localStoreData.tmall = extend(localStoreData.tmall,thisObj);
        localStorage["store-KPI-Data"] = JSON.stringify(localStoreData);
    }, function() {
        // console.log("Get Failed");
        console.log("%c Get Failed:","background:#fcc;color:#fff;font-size:120%;");
        if(needUpdate){
            if (!refresh) {
                getTmallAll();
                refresh = true;
            } else {
                location.reload();
            }
        }
        console.log(storeObj);
    }, 20000);
}